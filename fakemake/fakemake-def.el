;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'eix
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("eix-core"
                       "eix")
  site-lisp-config-prefix "50"
  license "GPL-2")
